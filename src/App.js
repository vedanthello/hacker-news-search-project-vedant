import React, { Component } from 'react';
import hackerNewsLogo from "./hacker-news-logo.png";
import './App.css';
import StoryList from "./components/StoryList.js";
import searchIcon from "./search-icon.svg";

class App extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      query: ""
    };
  }

  handleQueryChange = event => {
    this.setState(
      {
        query: event.target.value
      }
    );
  }

  render() {
    return (
      <div className="App">
  
        <header>
  
          <img src={hackerNewsLogo} alt="Hacker News Logo"/>  
  
          <div className="header-text">
            <h1>Search</h1>
            <h1>Hacker News</h1>
          </div>
  
          <div className="search-box">
            <img src={searchIcon} alt="Search icon" />
            <input 
              type="search"
              placeholder="Search stories by title, url or author"
              onChange={this.handleQueryChange} 
            />
          </div>
  
        </header>
  
        <StoryList query={this.state.query}/>
  
      </div>
    );
  }

}

export default App;
