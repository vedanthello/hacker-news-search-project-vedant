import React, { Component } from 'react';
import Story from "./Story.js";
import axios from "axios";

class StoryList extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      storyIds: [] 
    };
  }
  
  componentDidMount() {
    axios.get("https://hacker-news.firebaseio.com/v0/beststories.json")
    .then(response => {
      this.setState({storyIds: response.data.slice(0, 20)});
    })
    .catch(err => {
      console.log(err);
    });
  }

  render() {
    const {storyIds} = this.state;
    const storyList = storyIds.length ? (
      storyIds.map(storyId => <Story key={storyId} storyId={storyId} query={this.props.query} />) 
    ) : (
      null
    );

    return (
      <div className="StoryList">
        {storyList}
      </div>
    );
  }
}

export default StoryList;
