import React, { Component } from 'react';
import axios from "axios";
import { timeElapsed } from "../getTimeElapsed.js";

class Story extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      story: {},
      rendering: true 
    };
  }

  componentDidUpdate(prevProps) {

    const prevQuery = prevProps.query;
    const currentQuery = this.props.query;
    const queryRegex = new RegExp(currentQuery, "i");

    if (currentQuery !== prevQuery) {
      if (queryRegex.test(this.state.story.title) ||
          queryRegex.test(this.state.story.url) ||
          queryRegex.test(this.state.story.by)
      ) {
        this.setState({rendering: true});
      } else {
        this.setState({rendering: false});
      }
    }
  }

  componentDidMount() {
    axios.get(`https://hacker-news.firebaseio.com/v0/item/${this.props.storyId}.json`)
    .then(response => {
      this.setState({story: response.data});
    })
    .catch(err => {
      console.log(err);
    });
  }

  render() {

    const {
      id,
      title,
      url: sourceLink,
      score: pointCount,
      by: username,
      time: timePosted,
      descendants: commentCount,
      text
    } = this.state.story;
    
    const disussionLink = `https://news.ycombinator.com/item?id=${id}`;
    const userLink = `https://news.ycombinator.com/user?id=${username}`;

    return this.state.rendering && (
      <article className="Story">
        
        <div className="Story_title-and-source-link-wrapper">

          <a className="Story_title" href={disussionLink}>
            {title}
          </a>
          
          {
            (sourceLink === undefined) ? (
              null
            ) : (
            <a className="Story_source-link" href={sourceLink}>
              ({sourceLink})
            </a>  
            )
          }

        </div>

        <div className="Story_metadata">

          <a className="Story_points" href={disussionLink}>
            {pointCount} points,
          </a>

          <span className="Story_separator">
            |
          </span>

          <a className="Story_username" href={userLink}>
            {username}
          </a>

          <span className="Story_separator">
            |
          </span>

          <a className="Story_time-posted" href={disussionLink}>
            {timeElapsed(timePosted)}
          </a>

          <span className="Story_separator">
            |
          </span>

          <a className="Story_comment-count" href={disussionLink}>
            {commentCount} comments
          </a>
          
        </div>
  
      </article>
    );
  }
}

export default Story;

