import moment from "moment";

/**
 * Function parameters:
 * unixTimestamp - Unix timestamp
 * 
 * This function calculates the time interval between the given Unix timestamp
 * and the current time in either years, months, days, hours or minutes
 * and returns the same
 */
export const timeElapsed = unixTimestamp => {

  let interval = moment.unix(unixTimestamp).fromNow();
  let words = interval.split(" ");
   
  // convert "a minute ago" to "1 minute ago"
  // convert "an hour ago" to "1 hour ago"
  // and same for day, month and year
  // but leave "a few seconds ago" as it is
  if ((words[0] === "a" || words[0] === "an") && (words[1] !== "few")) {
    words[0] = "1";
    interval = words.join(" ");
  }

  return interval;
  
};